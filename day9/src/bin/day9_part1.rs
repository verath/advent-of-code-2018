fn main() {
    let input = shared::read_input_as_string();
    let (num_players, last_marble) = day9::parse_input(&input);
    let high_score = day9::play_game(num_players, last_marble);
    println!("{}", high_score);
}
