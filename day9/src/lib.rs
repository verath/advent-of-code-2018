use std::fmt;

#[derive(Debug)]
struct Marble {
    value: u64,
    next_idx: usize,
    prev_idx: usize,
}

struct Circle {
    marbles: Vec<Marble>,
    curr_idx: usize,
}

impl Circle {
    fn new() -> Circle {
        let mut circle = Circle {
            marbles: Vec::new(),
            curr_idx: 0,
        };
        circle.marbles.push(Marble {
            value: 0,
            prev_idx: 0,
            next_idx: 0,
        });
        circle
    }

    // Insert after current position, setting position to the newly
    // inserted value
    fn insert(&mut self, value: u64) {
        let next_idx = self.marbles[self.curr_idx].next_idx;
        let prev_idx = self.marbles[next_idx].prev_idx;
        let new_marble = Marble {
            value,
            prev_idx,
            next_idx,
        };
        let new_marble_idx = self.marbles.len();
        self.marbles.push(new_marble);
        self.marbles[prev_idx].next_idx = new_marble_idx;
        self.marbles[next_idx].prev_idx = new_marble_idx;
        self.curr_idx = new_marble_idx;
    }

    // Value of current position
    #[cfg(test)]
    fn get(&self) -> u64 {
        return self.marbles[self.curr_idx].value;
    }

    // Remove at current position, sets current position one step
    // clockwise
    fn remove(&mut self) -> u64 {
        let value = self.marbles[self.curr_idx].value;
        let next_idx = self.marbles[self.curr_idx].next_idx;
        let prev_idx = self.marbles[self.curr_idx].prev_idx;
        self.marbles[next_idx].prev_idx = prev_idx;
        self.marbles[prev_idx].next_idx = next_idx;
        self.curr_idx = next_idx;
        value
    }

    fn move_clockwise(&mut self, num_steps: usize) {
        for _ in 0..num_steps {
            self.curr_idx = self.marbles[self.curr_idx].next_idx;
        }
    }

    fn move_counter_clockwise(&mut self, num_steps: usize) {
        for _ in 0..num_steps {
            self.curr_idx = self.marbles[self.curr_idx].prev_idx;
        }
    }
}

impl fmt::Debug for Circle {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut idx = self.curr_idx;
        loop {
            write!(f, "{}", self.marbles[idx].value)?;
            idx = self.marbles[idx].next_idx;
            if idx == self.curr_idx {
                break;
            }
            write!(f, "->")?;
        }
        writeln!(f)
    }
}

pub fn parse_input(input: &str) -> (u64, u64) {
    assert!(input.is_ascii());
    let num_players = input
        .chars()
        .take_while(char::is_ascii_digit)
        .collect::<String>()
        .parse::<u64>()
        .unwrap();
    let last_marble = input
        .chars()
        .skip_while(char::is_ascii_digit)
        .skip_while(|ch| !ch.is_ascii_digit())
        .take_while(char::is_ascii_digit)
        .collect::<String>()
        .parse::<u64>()
        .unwrap();
    (num_players, last_marble)
}

pub fn play_game(num_players: u64, last_marble: u64) -> u64 {
    // Initial state
    let mut circle = Circle::new();
    let mut player_scores: Vec<u64> = vec![0; num_players as usize];

    for marble_value in 1..=last_marble {
        if marble_value % 23 == 0 {
            let player_idx = ((marble_value - 1) % num_players) as usize;
            let player_score = player_scores.get_mut(player_idx).unwrap();
            // Keep marble
            *player_score += marble_value;
            circle.move_counter_clockwise(7);
            *player_score += circle.remove();
        } else {
            circle.move_clockwise(1);
            circle.insert(marble_value);
        }
    }

    player_scores.into_iter().max().unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_circle() {
        let mut circle = Circle::new();
        circle.insert(3);
        circle.insert(5);
        circle.insert(8);
        assert_eq!(circle.get(), 8);

        circle.move_clockwise(1);
        assert_eq!(circle.get(), 0);

        circle.move_counter_clockwise(1);
        assert_eq!(circle.get(), 8);

        circle.move_clockwise(4);
        assert_eq!(circle.get(), 8);

        circle.move_counter_clockwise(4);
        assert_eq!(circle.get(), 8);

        assert_eq!(circle.remove(), 8);
        assert_eq!(circle.get(), 0);
        circle.move_counter_clockwise(1);
        assert_eq!(circle.get(), 5);
        circle.move_clockwise(2);
        assert_eq!(circle.get(), 3);
    }

    #[test]
    fn test_parse_input() {
        let (num_players, last_marble) =
            parse_input("10 players; last marble is worth 1618 points");
        assert_eq!((num_players, last_marble), (10, 1618));
    }

    #[test]
    fn day9_part1_examples() {
        let examples = vec![
            ("9 players; last marble is worth 25 points", 32),
            ("10 players; last marble is worth 1618 points", 8317),
            ("13 players; last marble is worth 7999 points", 146373),
            ("17 players; last marble is worth 1104 points", 2764),
            ("21 players; last marble is worth 6111 points", 54718),
            ("30 players; last marble is worth 5807 points", 37305),
        ];
        let input = examples
            .iter()
            .map(|&(input, expected)| (parse_input(input), expected));

        for ((num_players, last_marble), expected) in input {
            assert_eq!(play_game(num_players, last_marble), expected)
        }
    }
}
