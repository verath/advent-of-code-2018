use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

pub fn read_input() -> impl IntoIterator<Item = String> {
    let args: Vec<_> = env::args().collect();
    let file_path = args.get(1).expect("file_path param missing");
    let f = File::open(file_path).unwrap();
    let reader = BufReader::new(f);

    reader.lines().map(|l| l.unwrap())
}
