extern crate day2;

fn find_ids<'a, T>(box_ids: T) -> (&'a str, &'a str)
where
    T: IntoIterator<Item = &'a str>,
{
    let box_ids: Vec<&str> = box_ids.into_iter().collect();

    for (i, id_a) in box_ids.iter().enumerate() {
        let remaining_ids = &box_ids[i..];
        for id_b in remaining_ids {
            if common_letters(id_a, id_b).len() == (id_a.len() - 1) {
                return (id_a, id_b);
            }
        }
    }
    panic!("No ids!");
}

fn common_letters<'a>(word_a: &'a str, word_b: &'a str) -> String {
    assert!(word_a.len() == word_b.len());
    let word_a: Vec<char> = word_a.chars().collect();
    let word_b: Vec<char> = word_b.chars().collect();

    word_a
        .iter()
        .zip(word_b.iter())
        .filter(|(a, b)| a == b)
        .map(|(a, _)| a)
        .collect()
}

fn main() {
    let lines: Vec<String> = day2::read_input().into_iter().collect();
    let line_refs = lines.iter().map(String::as_ref);

    let (id_a, id_b) = find_ids(line_refs);
    println!("{}", common_letters(id_a, id_b))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_find_ids() {
        let box_ids = vec![
            "abcde", "fghij", "klmno", "pqrst", "fguij", "axcye", "wvxyz",
        ];
        assert_eq!(find_ids(box_ids), ("fghij", "fguij"))
    }

    #[test]
    fn test_common_letters() {
        let word_a = "fghij";
        let word_b = "fguij";
        assert_eq!(common_letters(word_a, word_b), "fgij");
    }
}
