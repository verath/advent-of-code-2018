extern crate day2;

use std::collections::HashMap;
use std::collections::HashSet;

/// Takes a string and returns a set where each entry in the set
/// represents that the original string had at least one char
/// that occured that many times.
fn build_uniq_lookup(s: &str) -> HashSet<u32> {
    let mut char_counts: HashMap<char, u32> = HashMap::new();
    for ch in s.chars() {
        let count = char_counts.entry(ch).or_insert(0);
        *count += 1;
    }
    let mut uniq_lookup = HashSet::new();
    for count in char_counts.values() {
        uniq_lookup.insert(*count);
    }
    uniq_lookup
}

/// Element-wise add two u32 tuples
fn add_tuples(t1: (u32, u32), t2: (u32, u32)) -> (u32, u32) {
    let (a1, a2) = t1;
    let (b1, b2) = t2;
    (a1 + b1, a2 + b2)
}

fn calc_checksum<'a, T>(box_ids: T) -> u32
where
    T: IntoIterator<Item = &'a str>,
{
    let (uniq_2s, uniq_3s) = box_ids
        .into_iter()
        .map(build_uniq_lookup)
        .map(|unique_lookup| {
            (
                unique_lookup.contains(&2) as u32,
                unique_lookup.contains(&3) as u32,
            )
        })
        .fold((0, 0), add_tuples);

    (uniq_2s * uniq_3s)
}

fn main() {
    let lines: Vec<String> = day2::read_input().into_iter().collect();
    let line_refs = lines.iter().map(String::as_ref);
    let checksum = calc_checksum(line_refs);
    println!("{}", checksum);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_add_tuples() {
        let (t1, t2) = ((0, 0), (0, 1));
        let expected = (0, 1);
        let actual = add_tuples(t1, t2);
        assert_eq!(actual, expected);

        let (t1, t2) = ((5, 5), (5, 5));
        let expected = (10, 10);
        let actual = add_tuples(t1, t2);
        assert_eq!(actual, expected);
    }

    #[test]
    fn test_build_uniq_lookup() {
        let lookup = build_uniq_lookup("aabbbcccc");
        assert!(lookup.contains(&2));
        assert!(lookup.contains(&3));
        assert!(lookup.contains(&4));
        assert!(!lookup.contains(&5));
        assert!(!lookup.contains(&1));
    }

    #[test]
    fn day2_part1_examples() {
        let input = vec![
            "abcdef", "bababc", "abbcde", "abcccd", "aabcdd", "abcdee", "ababab",
        ];
        let expected = 12;
        let actual = calc_checksum(input);
        assert_eq!(expected, actual);
    }
}
