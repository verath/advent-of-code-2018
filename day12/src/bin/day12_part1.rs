pub fn plant_char_to_bool(ch: char) -> bool {
    if ch == '#' {
        true
    } else if ch == '.' {
        false
    } else {
        panic!("ch was '{}', expected '#' or '.'!", ch);
    }
}

#[derive(Debug, Eq, PartialEq)]
struct SpreadPattern {
    pattern: [bool; 5],
    does_spread: bool,
}

impl SpreadPattern {
    pub fn from_string(s: &str) -> SpreadPattern {
        assert!(s.is_ascii());
        // "..#.. => ."
        let chars: Vec<char> = s.chars().collect();

        let mut pattern = [false; 5];
        for i in 0..5 {
            let ch = chars[i];
            pattern[i] = plant_char_to_bool(ch);
        }

        let ch = *chars.last().unwrap();
        let does_spread = plant_char_to_bool(ch);
        SpreadPattern {
            pattern,
            does_spread,
        }
    }

    // Returns:
    // - Some(true) if pattern matched and the current pot
    //   will have plant next generation.
    // - Some(false) if matches but pot will not have plant
    //   next generation.
    // - None if pattern did not match.
    pub fn apply(&self, pattern: &[bool]) -> Option<bool> {
        assert!(pattern.len() == 5);
        if self.pattern.eq(pattern) {
            Some(self.does_spread)
        } else {
            None
        }
    }
}

fn simulate_plants(
    initial_state: &[(i64, bool)],
    spread_patterns: &[SpreadPattern],
    num_iters: usize,
) -> Vec<(i64, bool)> {
    let mut state: Vec<(i64, bool)> = Vec::with_capacity(initial_state.len());
    for &s in initial_state {
        state.push(s);
    }

    for _ in 0..num_iters {
        // Make sure we have 5 empty pots on both sides
        let left_pad_size = 5 - state.iter().take_while(|&(_, v)| !v).take(5).count();
        let right_pad_size = 5 - state.iter().rev().take_while(|&(_, v)| !v).take(5).count();
        for _ in 0..left_pad_size {
            let low_idx = state[0].0;
            let empty_pot = (low_idx - 1, false);
            state.insert(0, empty_pot);
        }
        for _ in 0..right_pad_size {
            let high_idx = state[state.len() - 1].0;
            let empty_pot = (high_idx + 1, false);
            state.push(empty_pot);
        }

        // Apply spread patterns
        let end_idx = state.len() - 4;
        let mut next_state = state.clone();
        for i in 0..end_idx {
            let pattern: Vec<bool> = state
                .get(i..=i + 4)
                .unwrap()
                .iter()
                .map(|&(_, v)| v)
                .collect();

            let mut matched = false;
            for sp in spread_patterns {
                if let Some(v) = sp.apply(&pattern) {
                    next_state[i + 2].1 = v;
                    matched = true;
                    break;
                }
            }
            if !matched {
                next_state[i + 2].1 = false;
            }
        }
        state = next_state;
    }
    state
}

fn main() {
    let input = shared::read_input_as_string();
    let mut lines = input.lines();
    let initial_state = lines.next().unwrap();
    lines.next(); // empty line

    let spread_patterns: Vec<SpreadPattern> = lines.map(SpreadPattern::from_string).collect();
    let initial_state: Vec<(i64, bool)> = initial_state
        .chars()
        .skip("initial state: ".len())
        .map(plant_char_to_bool)
        .enumerate()
        .map(|(idx, val)| (idx as i64, val))
        .collect();

    let result = simulate_plants(&initial_state, &spread_patterns, 50000000000)
        .iter()
        .map(|&(idx, has_plant)| if has_plant { idx } else { 0 })
        .sum::<i64>();
    println!("{}", result);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_spread_pattern_from_string() {
        let input = "..#.. => .";
        let expected = SpreadPattern {
            pattern: [false, false, true, false, false],
            does_spread: false,
        };
        let actual = SpreadPattern::from_string(input);
        assert_eq!(expected, actual);

        let input = "##### => #";
        let expected = SpreadPattern {
            pattern: [true, true, true, true, true],
            does_spread: true,
        };
        let actual = SpreadPattern::from_string(input);
        assert_eq!(expected, actual);

        let input = "..... => .";
        let expected = SpreadPattern {
            pattern: [false, false, false, false, false],
            does_spread: false,
        };
        let actual = SpreadPattern::from_string(input);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_spread_pattern_apply() {
        let spread_pattern = SpreadPattern::from_string("##### => #");
        assert_eq!(
            spread_pattern.apply(&[true, true, true, true, true]),
            Some(true)
        );
        assert_eq!(spread_pattern.apply(&[true, false, true, true, true]), None);

        let spread_pattern = SpreadPattern::from_string("..... => .");
        assert_eq!(
            spread_pattern.apply(&[false, false, false, false, false]),
            Some(false)
        );
    }
}
