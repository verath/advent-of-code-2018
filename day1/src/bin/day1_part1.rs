extern crate day1;

fn apply_freq_updates<T: IntoIterator<Item = i64>>(updates: T) -> i64 {
    updates.into_iter().sum()
}

fn main() {
    let updates = day1::read_input();
    let frequency = apply_freq_updates(updates);
    println!("{}", frequency);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn day1_part1_examples() {
        let example = vec![1, -2, 3, 1];
        let expected = 3;
        assert_eq!(apply_freq_updates(example), expected);

        let example = vec![1, 1, 1];
        let expected = 3;
        assert_eq!(apply_freq_updates(example), expected);

        let example = vec![1, 1, -2];
        let expected = 0;
        assert_eq!(apply_freq_updates(example), expected);

        let example = vec![-1, -2, -3];
        let expected = -6;
        assert_eq!(apply_freq_updates(example), expected);
    }
}
