extern crate day1;

use std::collections::HashSet;

fn first_dup_freq<T: IntoIterator<Item = i64>>(updates: T) -> i64 {
    let updates: Vec<i64> = updates.into_iter().collect();

    let mut curr_freq = 0;
    let mut seen_freqs = HashSet::new();
    seen_freqs.insert(0);
    loop {
        for u in &updates {
            curr_freq += u;
            if !seen_freqs.insert(curr_freq) {
                // Already seen curr_freq
                return curr_freq;
            }
        }
    }
}

fn main() {
    let input = day1::read_input();
    println!("{}", first_dup_freq(input))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn day1_part2_examples() {
        let example = vec![1, -1];
        let expected = 0;
        assert_eq!(first_dup_freq(example), expected);

        let example = vec![3, 3, 4, -2, -4];
        let expected = 10;
        assert_eq!(first_dup_freq(example), expected);

        let example = vec![-6, 3, 8, 5, -6];
        let expected = 5;
        assert_eq!(first_dup_freq(example), expected);

        let example = vec![7, 7, -2, -7, -4];
        let expected = 14;
        assert_eq!(first_dup_freq(example), expected);
    }
}
