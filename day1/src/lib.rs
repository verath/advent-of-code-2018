use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

pub fn read_input() -> impl IntoIterator<Item = i64> {
    let args: Vec<String> = env::args().collect();
    let file_path = args.get(1).expect("file_path param missing");
    let f = File::open(file_path).unwrap();
    let reader = BufReader::new(f);
    // Read each line as an i64 number
    reader.lines().map(|l| l.unwrap().parse::<i64>().unwrap())
}
