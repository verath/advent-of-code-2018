fn day4_part2(log: Vec<String>) -> i64 {
    let log_records = day4::parse_log(log);
    let guards = day4::guards_from_records(log_records);

    let mut most_sleep = (0, 0, 0);
    for guard in guards {
        for minute in 0..=59 {
            let sleep_dur = guard.sleep_schedule[minute];
            if sleep_dur > most_sleep.1 {
                most_sleep = (minute, sleep_dur, guard.id);
            }
        }
    }
    most_sleep.0 as i64 * most_sleep.2 as i64
}

fn main() {
    let log = day4::read_input();
    println!("{}", day4_part2(log));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn day4_part2_example() {
        let log = vec![
            String::from("[1518-11-01 00:00] Guard #10 begins shift"),
            String::from("[1518-11-01 00:05] falls asleep"),
            String::from("[1518-11-01 00:25] wakes up"),
            String::from("[1518-11-01 00:30] falls asleep"),
            String::from("[1518-11-01 00:55] wakes up"),
            String::from("[1518-11-01 23:58] Guard #99 begins shift"),
            String::from("[1518-11-02 00:40] falls asleep"),
            String::from("[1518-11-02 00:50] wakes up"),
            String::from("[1518-11-03 00:05] Guard #10 begins shift"),
            String::from("[1518-11-03 00:24] falls asleep"),
            String::from("[1518-11-03 00:29] wakes up"),
            String::from("[1518-11-04 00:02] Guard #99 begins shift"),
            String::from("[1518-11-04 00:36] falls asleep"),
            String::from("[1518-11-04 00:46] wakes up"),
            String::from("[1518-11-05 00:03] Guard #99 begins shift"),
            String::from("[1518-11-05 00:45] falls asleep"),
            String::from("[1518-11-05 00:55] wakes up"),
        ];
        assert_eq!(day4_part2(log), 4455);
    }
}
