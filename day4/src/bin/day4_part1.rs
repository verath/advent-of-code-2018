extern crate day4;

fn day4_part1(log: Vec<String>) -> i64 {
    let log_records = day4::parse_log(log);
    let guards = day4::guards_from_records(log_records);
    let sleeper = guards
        .iter()
        .max_by(|&guard_a, &guard_b| {
            let total_sleep_a: u32 = guard_a.sleep_schedule.iter().sum();
            let total_sleep_b: u32 = guard_b.sleep_schedule.iter().sum();
            total_sleep_a.cmp(&total_sleep_b)
        })
        .unwrap();
    let most_sleep_min = sleeper
        .sleep_schedule
        .iter()
        .enumerate()
        .max_by(|(_, v1), (_, v2)| v1.cmp(&v2))
        .unwrap();
    let most_sleep_min = most_sleep_min.0;
    sleeper.id as i64 * most_sleep_min as i64
}

fn main() {
    let log = day4::read_input();
    println!("{}", day4_part1(log));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn day4_part1_example() {
        let log = vec![
            String::from("[1518-11-01 00:00] Guard #10 begins shift"),
            String::from("[1518-11-01 00:05] falls asleep"),
            String::from("[1518-11-01 00:25] wakes up"),
            String::from("[1518-11-01 00:30] falls asleep"),
            String::from("[1518-11-01 00:55] wakes up"),
            String::from("[1518-11-01 23:58] Guard #99 begins shift"),
            String::from("[1518-11-02 00:40] falls asleep"),
            String::from("[1518-11-02 00:50] wakes up"),
            String::from("[1518-11-03 00:05] Guard #10 begins shift"),
            String::from("[1518-11-03 00:24] falls asleep"),
            String::from("[1518-11-03 00:29] wakes up"),
            String::from("[1518-11-04 00:02] Guard #99 begins shift"),
            String::from("[1518-11-04 00:36] falls asleep"),
            String::from("[1518-11-04 00:46] wakes up"),
            String::from("[1518-11-05 00:03] Guard #99 begins shift"),
            String::from("[1518-11-05 00:45] falls asleep"),
            String::from("[1518-11-05 00:55] wakes up"),
        ];
        assert_eq!(day4_part1(log), 240);
    }
}
