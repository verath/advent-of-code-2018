use std::collections::HashMap;
use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

#[derive(Debug, PartialEq, Eq)]
pub enum LogRecord {
    // Holds the id of the guard.
    ShiftBegins(u16),
    // Holds the minute the guard fell asleep.
    FallsAsleep(u16),
    // Holds the minute the guard woke up.
    WakesUp(u16),
}

impl LogRecord {
    pub fn from_string(s: &str) -> LogRecord {
        if s.contains("begins shift") {
            // "[1518-11-04 00:02] Guard #99 begins shift"
            let mut guard_id = String::from("");
            for ch in s[26..].chars() {
                if !ch.is_ascii_digit() {
                    break;
                }
                guard_id.push(ch);
            }
            let guard_id = guard_id.parse::<u16>().unwrap();
            LogRecord::ShiftBegins(guard_id)
        } else {
            let minute = s[15..=16].parse::<u16>().unwrap();
            if s.contains("falls asleep") {
                // "[1518-11-04 00:36] falls asleep"
                LogRecord::FallsAsleep(minute)
            } else {
                // [1518-11-04 00:46] wakes up
                LogRecord::WakesUp(minute)
            }
        }
    }
}

pub struct Guard {
    pub id: u16,
    // Each index representing a minute, the value represents
    // how many times the guard was asleep at that minute.
    pub sleep_schedule: [u32; 60],
}

impl Guard {
    pub fn new(id: u16) -> Guard {
        Guard {
            id: id,
            sleep_schedule: [0; 60],
        }
    }
}

pub fn parse_log<T>(log: T) -> Vec<LogRecord>
where
    T: IntoIterator<Item = String>,
{
    let mut log: Vec<String> = log.into_iter().collect();
    log.sort();
    log.iter()
        .map(String::as_ref)
        .map(LogRecord::from_string)
        .collect()
}

pub fn guards_from_records<T>(log_records: T) -> Vec<Guard>
where
    T: IntoIterator<Item = LogRecord>,
{
    let mut guards: HashMap<u16, Guard> = HashMap::new();
    let mut asleep_at: Option<u16> = None;
    let mut curr_id: Option<u16> = None;
    for record in log_records {
        match record {
            LogRecord::ShiftBegins(id) => {
                guards.entry(id).or_insert(Guard::new(id));
                curr_id = Some(id);
            }
            LogRecord::FallsAsleep(minute) => asleep_at = Some(minute),
            LogRecord::WakesUp(awake_at) => {
                let asleep_at = asleep_at.take().unwrap() as usize;
                let awake_at = awake_at as usize;
                let ref mut guard = guards.get_mut(&curr_id.unwrap()).unwrap();
                for min in asleep_at..awake_at {
                    guard.sleep_schedule[min] += 1;
                }
            }
        }
    }
    guards.into_iter().map(|(_k, v)| v).collect()
}

pub fn read_input() -> Vec<String> {
    let args: Vec<String> = env::args().collect();
    let file_path = args.get(1).expect("file_path param missing");
    let f = File::open(file_path).unwrap();
    BufReader::new(f).lines().map(Result::unwrap).collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_log_record_from_string() {
        let actual = LogRecord::from_string(&String::from("[1518-11-01 00:05] falls asleep"));
        let expected = LogRecord::FallsAsleep(05);
        assert_eq!(actual, expected);

        let actual =
            LogRecord::from_string(&String::from("[1518-11-01 00:00] Guard #10 begins shift"));
        let expected = LogRecord::ShiftBegins(10);
        assert_eq!(actual, expected);

        let actual = LogRecord::from_string(&String::from("[1518-11-01 00:25] wakes up"));
        let expected = LogRecord::WakesUp(25);
        assert_eq!(actual, expected);
    }

    #[test]
    fn test_parse_log() {
        let log = vec![
            String::from("[1518-11-01 00:05] falls asleep"),
            String::from("[1518-11-01 00:00] Guard #10 begins shift"),
            String::from("[1518-11-01 00:25] wakes up"),
        ];
        let actual = parse_log(log);
        let expected = vec![
            LogRecord::ShiftBegins(10),
            LogRecord::FallsAsleep(5),
            LogRecord::WakesUp(25),
        ];
        assert_eq!(actual, expected);
    }
}
