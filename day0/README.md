# Day "0" - Example project structure

An example project structure for a rust project per day,
where each day has 2 parts. Each part is a separate binary
and shared code is put in lib.

## Create
```
> cargo new --vcs none ./day0
> cd day0
> rm main.rs
> mkdir bin
> touch bin/day0_part1.rs
> touch bin/day0_part2.rs
> touch lib.rs
```

## Run

```
> cargo run --bin day0_part1
> cargo run --bin day0_part2
```
