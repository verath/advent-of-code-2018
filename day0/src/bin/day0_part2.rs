extern crate day0;

use day0::print_part;

fn main() {
    print_part("part2");
    println!("... And some other stuff.");
}
