fn main() {
    let grid_serial = 5177;
    let size = (300, 300);

    // Improvement: Calculate cell powers once instead of for each iter.
    let mut highest_power = std::i64::MIN;
    let mut highest_power_pos = (0, 0);
    let mut highest_power_size = 0;
    for sq_size in 1..=300 {
        let sq_size = (sq_size, sq_size);
        let (power, pos) = day11::most_power(size, sq_size, grid_serial);
        if power > highest_power {
            highest_power = power;
            highest_power_pos = pos;
            highest_power_size = sq_size.0;
        }
    }
    println!("{:?},{}", highest_power_pos, highest_power_size);
}
