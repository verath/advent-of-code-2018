fn main() {
    let grid_serial = 5177;
    let size = (300, 300);
    let sq_size = (3, 3);
    let (_, pos) = day11::most_power(size, sq_size, grid_serial);
    println!("{:?}", pos);
}
