fn cell_power((x, y): (u64, u64), grid_serial: u64) -> i64 {
    let rack_id = x + 10;
    let mut power_level = rack_id * y;
    power_level += grid_serial;
    power_level *= rack_id;
    power_level = (power_level / 100) % 10;
    (power_level as i64) - 5
}

fn area_power((min_x, min_y): (u64, u64), (width, height): (u64, u64), grid_serial: u64) -> i64 {
    let max_x = min_x + width;
    let max_y = min_y + height;
    let mut total_power = 0;
    for y in min_y..max_y {
        for x in min_x..max_x {
            total_power += cell_power((x, y), grid_serial);
        }
    }
    total_power
}

pub fn most_power(
    (width, height): (u64, u64),
    (square_width, square_height): (u64, u64),
    grid_serial: u64,
) -> (i64, (u64, u64)) {
    let min_x = 1;
    let min_y = 1;
    let max_x = width - square_width + 1;
    let max_y = height - square_height + 1;

    let mut highest_power = std::i64::MIN;
    let mut highest_power_pos = (0, 0);
    for y in min_y..=max_y {
        for x in min_x..=max_x {
            let pos = (x, y);
            let power = area_power(pos, (square_width, square_height), grid_serial);
            if power > highest_power {
                highest_power = power;
                highest_power_pos = pos;
            }
        }
    }
    (highest_power, highest_power_pos)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_cell_power() {
        assert_eq!(cell_power((3, 5), 8), 4);
        assert_eq!(cell_power((122, 79), 57), -5);
        assert_eq!(cell_power((217, 196), 39), 0);
        assert_eq!(cell_power((101, 153), 71), 4);
    }

    #[test]
    fn test_area_power() {
        let pos = (33, 45);
        let size = (3, 3);
        let grid_serial = 18;
        let expected = 29;
        assert_eq!(area_power(pos, size, grid_serial), expected);

        let pos = (21, 61);
        let size = (3, 3);
        let grid_serial = 42;
        let expected = 30;
        assert_eq!(area_power(pos, size, grid_serial), expected);
    }

    #[test]
    fn test_most_power() {
        let grid_serial = 42;
        let expected = (21, 61);
        let (_, actual) = most_power((300, 300), (3, 3), grid_serial);
        assert_eq!(actual, expected);

        let grid_serial = 18;
        let expected = (33, 45);
        let (_, actual) = most_power((300, 300), (3, 3), grid_serial);
        assert_eq!(actual, expected);
    }
}
