#[macro_use]
extern crate lazy_static;
extern crate regex;

use regex::Regex;

#[derive(Debug, Eq, PartialEq)]
struct Entry {
    position: (i64, i64),
    velocity: (i64, i64),
}

fn entry_from_string(s: &str) -> Entry {
    lazy_static! {
        static ref RE: Regex = Regex::new(
            r"position=<\s*(?P<pos_x>-?\d+),\s+(?P<pos_y>-?\d+)> velocity=<\s*(?P<vel_x>-?\d+),\s+(?P<vel_y>-?\d+)"
        )
        .unwrap();
    }
    let caps = RE.captures(s).unwrap();
    Entry {
        position: (
            caps["pos_x"].parse::<i64>().unwrap(),
            caps["pos_y"].parse::<i64>().unwrap(),
        ),
        velocity: (
            caps["vel_x"].parse::<i64>().unwrap(),
            caps["vel_y"].parse::<i64>().unwrap(),
        ),
    }
}

fn size(entries: &Vec<Entry>) -> ((i64, i64), (i64, i64)) {
    let (mut min_x, mut min_y) = (std::i64::MAX, std::i64::MAX);
    let (mut max_x, mut max_y) = (std::i64::MIN, std::i64::MIN);
    for entry in entries {
        let (x, y) = entry.position;
        if x < min_x {
            min_x = x;
        }
        if y < min_y {
            min_y = y;
        }
        if x > max_x {
            max_x = x;
        }
        if y > max_y {
            max_y = y;
        }
    }
    ((min_x, min_y), (max_x, max_y))
}

fn calc_area(entries: &Vec<Entry>) -> i64 {
    let ((min_x, min_y), (max_x, max_y)) = size(entries);
    (max_x - min_x).abs() * (max_y - min_y).abs()
}

fn move_entries(entries: &mut Vec<Entry>) {
    for entry in entries {
        let (mut x, mut y) = entry.position;
        let (vx, vy) = entry.velocity;
        x += vx;
        y += vy;
        entry.position = (x, y);
    }
}

// :)
fn move_entries_back(entries: &mut Vec<Entry>) {
    for entry in entries {
        let (mut x, mut y) = entry.position;
        let (vx, vy) = entry.velocity;
        x -= vx;
        y -= vy;
        entry.position = (x, y);
    }
}

fn entry_exist(position: (i64, i64), entries: &Vec<Entry>) -> bool {
    for entry in entries {
        if position == entry.position {
            return true;
        }
    }
    false
}

fn print_smallest_area(entries: &mut Vec<Entry>) {
    let mut prev_area = std::i64::MAX;
    let mut elapsed_seconds = 0;
    loop {
        let area = calc_area(entries);
        if area > prev_area {
            move_entries_back(entries);
            elapsed_seconds -= 1;
            break;
        }
        prev_area = area;
        move_entries(entries);
        elapsed_seconds += 1;
    }
    println!("After {} seconds:", elapsed_seconds);

    let ((min_x, min_y), (max_x, max_y)) = size(entries);
    for y in min_y..=max_y {
        for x in min_x..=max_x {
            if entry_exist((x, y), entries) {
                print!("{}", "X");
            } else {
                print!("{}", ".");
            }
        }
        println!();
    }
}

fn main() {
    let input = shared::read_input_as_string();
    let mut entries: Vec<Entry> = input.lines().map(entry_from_string).collect();

    print_smallest_area(&mut entries);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_entry_from_string() {
        let input = "position=<-32665,  43460> velocity=<-3, -4>";
        let expected = Entry {
            position: (-32665, 43460),
            velocity: (-3, -4),
        };
        assert_eq!(entry_from_string(&input), expected);
    }
}
