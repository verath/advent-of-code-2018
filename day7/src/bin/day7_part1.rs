use std::collections::HashSet;

// A requires B -> (A, Some(B)).
// The required node is an Option to allow in-place resolving
// dependencies by setting the required node to None.
type Dependency = (char, Option<char>);

fn dependency_from_string(s: &str) -> Dependency {
    let s = s.as_bytes();
    // "Step A must be finished before step B can begin."
    let req_node: char = s[5] as char;
    let dep_node: char = s[36] as char;
    (dep_node, Some(req_node))
}

fn resolve_order<D>(deps: D) -> String
where
    D: IntoIterator<Item = Dependency>,
{
    let mut deps: Vec<Dependency> = deps.into_iter().collect();

    // Find all unique nodes
    let nodes: HashSet<char> = deps
        .iter()
        .map(|&(ch, _)| ch)
        .chain(deps.iter().filter_map(|&(_, ch)| ch))
        .collect();
    // -> Vec for sort, Option to represent resolved nodes by None.
    let mut nodes: Vec<Option<char>> = nodes.into_iter().map(|n| Some(n)).collect();

    // Sort nodes, to resolve in alphabetical order
    nodes.sort();

    let mut resolved_nodes: Vec<char> = vec![];
    loop {
        // Find first node without any dependencies
        let node = nodes.iter().filter_map(|&n| n).find(|&ch| {
            deps.iter()
                .all(|&(ch_dep, ch_req)| ch_dep != ch || ch_req.is_none())
        });

        if let Some(node_ch) = node {
            resolved_nodes.push(node_ch);
            // Remove resolved node from the nodes vec
            nodes
                .iter_mut()
                .find(|n| n.map_or(false, |ch| ch == node_ch))
                .unwrap()
                .take();
            // Remove resolved node from dependency pairs
            deps.iter_mut()
                .filter(|(_, req_node)| req_node.map_or(false, |ch| ch == node_ch))
                .for_each(|(_, req_node)| {
                    req_node.take();
                });
        } else {
            break;
        }
    }
    resolved_nodes.iter().collect()
}

fn main() {
    let input = shared::read_input_as_string();
    let deps = input.lines().map(dependency_from_string);
    println!("{}", resolve_order(deps));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_dependency_from_string() {
        let expected = ('B', Some('A'));
        let actual = dependency_from_string("Step A must be finished before step B can begin.");
        assert_eq!(actual, expected);
    }

    #[test]
    fn test_resolve_order() {
        let deps: Vec<Dependency> = vec![
            "Step C must be finished before step A can begin.",
            "Step C must be finished before step F can begin.",
            "Step A must be finished before step B can begin.",
            "Step A must be finished before step D can begin.",
            "Step B must be finished before step E can begin.",
            "Step D must be finished before step E can begin.",
            "Step F must be finished before step E can begin.",
        ]
        .into_iter()
        .map(dependency_from_string)
        .collect();
        assert_eq!(resolve_order(deps), "CABDFE")
    }
}
