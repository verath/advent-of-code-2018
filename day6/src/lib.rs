use std::env;
use std::fs::File;
use std::io::prelude::*;

pub type Coordinate = (u16, u16);

pub trait FromString<T> {
    fn from_string(&str) -> T;
}

impl FromString<Coordinate> for Coordinate {
    fn from_string(s: &str) -> Coordinate {
        let mut splitted = s.split(", ");
        let x = splitted.next().unwrap().parse::<u16>().unwrap();
        let y = splitted.next().unwrap().parse::<u16>().unwrap();
        (x, y)
    }
}

pub fn manhattan_distance(&(x1, y1): &Coordinate, &(x2, y2): &Coordinate) -> u32 {
    let diff_x = (x1 as i32 - x2 as i32).abs();
    let diff_y = (y1 as i32 - y2 as i32).abs();
    (diff_x + diff_y) as u32
}

pub fn read_input() -> String {
    let args: Vec<String> = env::args().collect();
    let file_path = args.get(1).expect("file_path param missing");
    let mut f = File::open(file_path).unwrap();

    let mut input = String::new();
    f.read_to_string(&mut input).unwrap();
    input.trim_end().to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_coordinate_from_string() {
        assert_eq!(Coordinate::from_string("124, 262"), (124, 262));
        assert_eq!(Coordinate::from_string("80, 156"), (80, 156));
    }

    #[test]
    fn test_manhattan_distance() {
        assert_eq!(manhattan_distance(&(0, 0), &(0, 1)), 1);
        assert_eq!(manhattan_distance(&(0, 0), &(1, 0)), 1);
        assert_eq!(manhattan_distance(&(0, 1), &(0, 0)), 1);
        assert_eq!(manhattan_distance(&(1, 0), &(0, 0)), 1);
        assert_eq!(manhattan_distance(&(1, 1), &(8, 9)), 15);
    }
}
