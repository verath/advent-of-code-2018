extern crate day6;

use day6::{Coordinate, FromString};

fn day6_part2<P>(coords: P, max_distance: u32) -> u32
where
    P: IntoIterator<Item = Coordinate>,
{
    let coords: Vec<Coordinate> = coords.into_iter().collect();

    // Find a region that holds all coords
    let (mut max_x, mut max_y) = (0, 0);
    let (mut min_x, mut min_y) = (1000, 1000);
    for &(x, y) in &coords {
        if x < min_x {
            min_x = x;
        }
        if x > max_x {
            max_x = x;
        }
        if y < min_y {
            min_y = y;
        }
        if y > max_y {
            max_y = y;
        }
    }

    // Expand the region by max_distance, to make sure we have all
    // possible coords in the grid
    let (max_x, max_y) = (
        max_x as i32 + max_distance as i32,
        max_y as i32 + max_distance as i32,
    );
    let (min_x, min_y) = (
        min_x as i32 - max_distance as i32,
        min_y as i32 - max_distance as i32,
    );

    let mut valid_area = 0;
    for x in min_x..=max_x {
        for y in min_y..=max_y {
            let mut total_distance = 0;
            for coord in &coords {
                total_distance += day6::manhattan_distance(coord, &(x as u16, y as u16));
                if total_distance >= max_distance {
                    break;
                }
            }
            if total_distance < max_distance {
                valid_area += 1;
            }
        }
    }
    valid_area
}

fn main() {
    let input = day6::read_input();
    let coords = input.lines().map(Coordinate::from_string);
    println!("{}", day6_part2(coords, 10000))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn day6_part1_example() {
        let coords: Vec<Coordinate> = vec![(1, 1), (1, 6), (8, 3), (3, 4), (5, 5), (8, 9)];
        let area = day6_part2(coords, 32);
        assert_eq!(area, 16);
    }
}
