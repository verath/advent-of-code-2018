extern crate day6;

use std::collections::HashMap;
use std::collections::HashSet;

use day6::{manhattan_distance, Coordinate, FromString};

#[derive(Debug)]
enum Location {
    Empty,
    Owned { id: u8 },
    Closest { id: u8, distance: u32 },
    Contested { distance: u32 },
}

fn edge_coords(
    (min_x, min_y): Coordinate,
    (max_x, max_y): Coordinate,
) -> impl Iterator<Item = Coordinate> {
    ((min_x + 1)..max_x)
        .map(move |x| (x, min_y))
        .chain(((min_x + 1)..max_x).map(move |x| (x, max_y)))
        .chain(((min_y + 1)..max_y).map(move |y| (min_x, y)))
        .chain(((min_y + 1)..max_y).map(move |y| (max_x, y)))
}

fn largest_area<P>(coords: P) -> u32
where
    P: IntoIterator<Item = Coordinate>,
{
    use Location::*;

    let owned_coords: Vec<(usize, Coordinate)> = coords.into_iter().enumerate().collect();
    let (mut max_x, mut max_y) = (0, 0);
    let (mut min_x, mut min_y) = (1000, 1000);
    let mut grid: HashMap<Coordinate, Location> = HashMap::new();

    // Find size of grid, place "Owned" entires
    for &(idx, coord) in &owned_coords {
        let (x, y) = coord;
        if x > max_x {
            max_x = x;
        }
        if x < min_x {
            min_x = x;
        }
        if y > max_y {
            max_y = y;
        }
        if y < min_y {
            min_y = y;
        }
        grid.insert(coord, Location::Owned { id: idx as u8 });
    }

    // Place "Empty" entries
    grid.reserve((max_x - min_x) as usize * (max_y - min_y) as usize);
    for x in min_x..=max_x {
        for y in min_y..=max_y {
            grid.entry((x, y)).or_insert(Location::Empty);
        }
    }

    // Calculate and place "Closest" and/or "Contested" entries
    for (idx, owned_coord) in &owned_coords {
        for (coord, location) in &mut grid {
            let new_distance = manhattan_distance(owned_coord, coord);
            let new_loc = match location {
                Empty => Some(Closest {
                    id: *idx as u8,
                    distance: new_distance,
                }),
                Closest { distance, .. } => {
                    if new_distance < *distance {
                        Some(Closest {
                            id: *idx as u8,
                            distance: new_distance,
                        })
                    } else if new_distance == *distance {
                        Some(Contested {
                            distance: new_distance,
                        })
                    } else {
                        None
                    }
                }
                Contested { distance } if new_distance < *distance => Some(Closest {
                    id: *idx as u8,
                    distance: new_distance,
                }),
                _ => None,
            };
            if let Some(new_loc) = new_loc {
                *location = new_loc;
            }
        }
    }

    // Find all ids of locations that continues for infinity by
    // looking at all the edges of the grid
    let infinite_ids: HashSet<u8> = edge_coords((min_x, min_y), (max_x, max_y))
        .filter_map(|coord| match grid.get(&coord).unwrap() {
            Owned { id } => Some(*id),
            Closest { id, .. } => Some(*id),
            _ => None,
        })
        .collect();

    owned_coords
        .iter()
        .filter(|&&(idx, _)| !infinite_ids.contains(&(idx as u8)))
        .map(|&(idx, _)| {
            // Count number of locations owned by id
            let id = idx as u8;
            grid.iter()
                .filter(|&(_, location)| match location {
                    &Owned { id: owner_id } => owner_id == id,
                    &Closest { id: owner_id, .. } => owner_id == id,
                    _ => false,
                })
                .count()
        })
        .max()
        .unwrap() as u32
}

fn main() {
    let input = day6::read_input();
    let coords = input.lines().map(Coordinate::from_string);
    println!("{}", largest_area(coords))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn day6_part1_example() {
        let coords: Vec<Coordinate> = vec![(1, 1), (1, 6), (8, 3), (3, 4), (5, 5), (8, 9)];
        let area = largest_area(coords);
        assert_eq!(area, 17);
    }
}
