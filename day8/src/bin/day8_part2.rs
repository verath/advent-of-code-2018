use std::str::FromStr;

fn node_value_rec(tree: &[u8]) -> (&[u8], u32) {
    let num_children = tree[0] as usize;
    let num_metadata = tree[1] as usize;

    // Recurse into child nodes, consuming the tree.
    let mut tree = &tree[2..];
    let mut child_values = vec![];
    for _ in 0..num_children {
        let (remaining_tree, value) = node_value_rec(tree);
        child_values.push(value);
        tree = remaining_tree;
    }

    // Extract metadata entries.
    let metadata = tree[..num_metadata].into_iter().map(|&n| u32::from(n));
    let tree = &tree[num_metadata..];

    // Calculate node value. If no children, the value is the sum of
    // the metadata. Otherwise the metadata acts as indices into the
    // child_values, starting at 1 for the first node.
    if num_children == 0 {
        (tree, metadata.sum::<u32>())
    } else {
        let value = metadata
            .map(|idx| (idx - 1) as usize)
            .map(|idx| child_values.get(idx))
            .map(|v| v.unwrap_or(&0))
            .sum::<u32>();
        (tree, value)
    }
}

fn node_value(tree: &[u8]) -> u32 {
    let (_, value) = node_value_rec(tree);
    value
}

fn main() {
    let input = shared::read_input_as_string();
    let input: Vec<u8> = input
        .split(' ')
        .map(u8::from_str)
        .map(Result::unwrap)
        .collect();
    println!("{}", node_value(&input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_tree_value() {
        let input: Vec<u8> = "2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2"
            .split(' ')
            .map(u8::from_str)
            .map(Result::unwrap)
            .collect();
        assert_eq!(node_value(&input), 66);
    }
}
