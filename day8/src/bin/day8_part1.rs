use std::str::FromStr;

fn sum_metadata_rec(input: &[u8]) -> (&[u8], u32) {
    let num_children = input[0] as usize;
    let num_metadata = input[1] as usize;

    // Recurse into child nodes
    let mut input = &input[2..];
    let mut metadata_sum = 0;
    for _ in 0..num_children {
        let (remaining_input, child_metadata_sum) = sum_metadata_rec(input);
        metadata_sum += child_metadata_sum;
        input = remaining_input;
    }

    // Add current node metadata
    metadata_sum += &input[..num_metadata]
        .into_iter()
        .map(|&n| u32::from(n))
        .sum::<u32>();
    (&input[num_metadata..], metadata_sum)
}

fn sum_metadata(input: &[u8]) -> u32 {
    let (_, metadata_sum) = sum_metadata_rec(input);
    metadata_sum
}

fn main() {
    let input = shared::read_input_as_string();
    let input: Vec<u8> = input
        .split(' ')
        .map(u8::from_str)
        .map(Result::unwrap)
        .collect();
    println!("{}", sum_metadata(&input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sum_metadata() {
        let input: Vec<u8> = "2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2"
            .split(' ')
            .map(u8::from_str)
            .map(Result::unwrap)
            .collect();
        assert_eq!(sum_metadata(&input), 138);
    }
}
