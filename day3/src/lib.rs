#[macro_use]
extern crate lazy_static;
extern crate regex;

use regex::Regex;
use std::collections::HashSet;
use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

// left, top, width, height
#[derive(Eq, PartialEq, Clone, Debug)]
pub struct Claim {
    pub id: u32,
    pub left: u32,
    pub top: u32,
    pub width: u32,
    pub height: u32,
}

impl Claim {
    pub fn from_string(s: &str) -> Claim {
        lazy_static! {
            static ref RE: Regex = Regex::new(
                r"#(?P<id>\d+) @ (?P<left>\d+),(?P<top>\d+): (?P<width>\d+)x(?P<height>\d+)"
            )
            .unwrap();
        }
        let caps = RE.captures(s).expect("String did not match a Claim");
        Claim {
            id: caps["id"].parse::<u32>().unwrap(),
            left: caps["left"].parse::<u32>().unwrap(),
            top: caps["top"].parse::<u32>().unwrap(),
            width: caps["width"].parse::<u32>().unwrap(),
            height: caps["height"].parse::<u32>().unwrap(),
        }
    }

    pub fn overlaps(&self, other: &Claim) -> bool {
        // Yikes!
        let mut canvas = HashSet::new();
        for x in self.left..(self.left + self.width) {
            for y in self.top..(self.top + self.height) {
                let coord = (x, y);
                canvas.insert(coord);
            }
        }
        for x in other.left..(other.left + other.width) {
            for y in other.top..(other.top + other.height) {
                let coord = (x, y);
                if canvas.contains(&coord) {
                    return true;
                }
            }
        }
        false
    }
}

pub fn read_input() -> impl IntoIterator<Item = Claim> {
    let args: Vec<String> = env::args().collect();
    let file_path = args.get(1).expect("file_path param missing");
    let f = File::open(file_path).unwrap();
    BufReader::new(f)
        .lines()
        .map(Result::unwrap)
        .map(|s| Claim::from_string(&s))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_claim_from_string() {
        let claim_str = "#1 @ 258,327: 19x22";
        let actual = Claim::from_string(claim_str);
        let expected = Claim {
            id: 1,
            left: 258,
            top: 327,
            width: 19,
            height: 22,
        };
        assert_eq!(actual, expected);
    }

    #[test]
    fn test_claim_overlaps() {
        let c1 = Claim::from_string("#1 @ 0,0: 1x1");
        let c2 = Claim::from_string("#2 @ 0,0: 1x1");
        assert!(c1.overlaps(&c2));
        assert!(c2.overlaps(&c1));

        let c1 = Claim::from_string("#1 @ 0,0: 1x1");
        let c2 = Claim::from_string("#2 @ 10,10: 1x1");
        assert!(!c1.overlaps(&c2));
        assert!(!c2.overlaps(&c1));

        let c1 = Claim::from_string("#1 @ 1,3: 4x4");
        let c2 = Claim::from_string("#3 @ 5,5: 2x2");
        assert!(!c1.overlaps(&c2));
        assert!(!c2.overlaps(&c1));
    }
}
