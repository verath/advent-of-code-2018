extern crate day3;

use day3::Claim;

fn find_intact_claim<T>(claims: T) -> Option<Claim>
where
    T: IntoIterator<Item = Claim>,
{
    let claims: Vec<Claim> = claims.into_iter().collect();
    for claim in &claims {
        let mut is_intact = true;
        for other_claim in &claims {
            if claim == other_claim {
                continue;
            }
            if claim.overlaps(other_claim) {
                is_intact = false;
                break;
            }
        }
        if is_intact {
            return Some(claim.clone());
        }
    }
    None
}

fn main() {
    let claims = day3::read_input();
    let intact_claim = find_intact_claim(claims).unwrap();
    println!("{}", intact_claim.id);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn day3_part2_example() {
        let claims = vec!["#1 @ 1,3: 4x4", "#2 @ 3,1: 4x4", "#3 @ 5,5: 2x2"];
        let claims = claims.iter().map(|&s| Claim::from_string(s));
        assert_eq!(3, find_intact_claim(claims).unwrap().id);
    }
}
