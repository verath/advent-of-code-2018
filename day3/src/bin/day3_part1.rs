extern crate day3;

use day3::Claim;

use std::collections::HashMap;

fn count_claim_overlaps<T>(claims: T) -> u32
where
    T: IntoIterator<Item = Claim>,
{
    // (x, y) -> num claims
    let mut fabric: HashMap<(u32, u32), u32> = HashMap::new();
    let mut num_overlaps = 0;
    for claim in claims {
        let Claim {
            id: _,
            left,
            top,
            width,
            height,
        } = claim;
        for x in left..(left + width) {
            for y in top..(top + height) {
                let coords = (x, y);
                let mut entry = fabric.entry(coords).or_insert(0);
                *entry += 1;
                if *entry == 2 {
                    num_overlaps += 1;
                }
            }
        }
    }
    num_overlaps
}

fn main() {
    let claims = day3::read_input();
    let num_overlaps = count_claim_overlaps(claims);
    println!("{}", num_overlaps);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn day3_part1_example() {
        let claims = vec!["#1 @ 1,3: 4x4", "#2 @ 3,1: 4x4", "#3 @ 5,5: 2x2"];
        let claims = claims.iter().map(|&s| Claim::from_string(s));
        assert_eq!(4, count_claim_overlaps(claims));
    }
}
