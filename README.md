# Advent of Code 2018

Language: Rust

## Run a binary:
```
cargo run --bin day1_part1 day/input.txt
```
