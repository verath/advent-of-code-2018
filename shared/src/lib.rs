use std::env;
use std::fs::File;
use std::io::prelude::*;

pub fn read_input_as_string() -> String {
    let args: Vec<String> = env::args().collect();
    let file_path = args.get(1).expect("file_path param missing");
    let mut f = File::open(file_path).unwrap();

    let mut input = String::new();
    f.read_to_string(&mut input).unwrap();
    input.trim_end().to_string()
}
