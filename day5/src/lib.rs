use std::env;
use std::fs::File;
use std::io::prelude::*;

pub fn read_input() -> String {
    let args: Vec<String> = env::args().collect();
    let file_path = args.get(1).expect("file_path param missing");
    let mut f = File::open(file_path).unwrap();

    let mut input = String::new();
    f.read_to_string(&mut input).unwrap();
    input.trim_end().to_string()
}

fn does_react(ch1: char, ch2: char) -> bool {
    let is_diff_case =
        ch1.is_uppercase() && ch2.is_lowercase() || ch1.is_lowercase() && ch2.is_uppercase();
    if is_diff_case {
        ch1.to_ascii_uppercase() == ch2.to_ascii_uppercase()
    } else {
        false
    }
}

pub fn react(polymer: &str) -> impl IntoIterator<Item = char> {
    let mut polymer_chars: Vec<char> = polymer.chars().collect();
    loop {
        let mut did_react = false;
        let mut i = (polymer_chars.len() - 1) as i32;
        while i > 0 {
            let idx1 = i as usize;
            let idx2 = (i - 1) as usize;
            let ch1 = *polymer_chars.get(idx1).unwrap();
            let ch2 = *polymer_chars.get(idx2).unwrap();
            if does_react(ch1, ch2) {
                did_react = true;
                polymer_chars.remove(idx1);
                polymer_chars.remove(idx2);
                // idx2 now also invalid, skip
                i -= 1;
            }
            i -= 1;
        }
        if !did_react || polymer_chars.is_empty() {
            break;
        }
    }
    polymer_chars
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_does_react() {
        assert!(!does_react('a', 'a'));
        assert!(does_react('a', 'A'));
        assert!(does_react('A', 'a'));
        assert!(!does_react('A', 'A'));
    }

    #[test]
    fn test_react() {
        fn react_wrap(s: &str) -> String {
            react(s).into_iter().collect()
        }

        assert_eq!(react_wrap("aA"), "");
        assert_eq!(react_wrap("abBA"), "");
        assert_eq!(react_wrap("abAB"), "abAB");
        assert_eq!(react_wrap("aabAAB"), "aabAAB");
        assert_eq!(react_wrap("dabAcCaCBAcCcaDA"), "dabCBAcaDA");
    }
}
