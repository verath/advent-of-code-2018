extern crate day5;

fn day5_part1(polymer: &str) -> u32 {
    let polymer = day5::react(polymer);
    polymer.into_iter().count() as u32
}

fn main() {
    println!("{}", day5_part1(&day5::read_input()));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn day5_part1_examples() {
        assert_eq!(day5_part1("dabAcCaCBAcCcaDA"), 10);
    }
}
