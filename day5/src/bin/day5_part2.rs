use std::collections::HashSet;
use std::sync::{Arc, Mutex};
use std::thread;

fn day5_part2(polymer: &str) -> u32 {
    let uniq_chars: HashSet<char> = polymer.chars().map(|ch| ch.to_ascii_lowercase()).collect();

    let min_polymer = Arc::new(Mutex::new(std::u32::MAX));
    let thread_handles: Vec<thread::JoinHandle<_>> = uniq_chars
        .iter()
        .map(|&ch| {
            let polymer: String = polymer
                .chars()
                .filter(|poly_ch| poly_ch.to_ascii_lowercase() != ch)
                .collect();
            let min_polymer = Arc::clone(&min_polymer);
            std::thread::spawn(move || {
                let polymer_size = day5::react(&polymer).into_iter().count() as u32;
                let mut min_polymer = min_polymer.lock().unwrap();
                if polymer_size < *min_polymer {
                    *min_polymer = polymer_size;
                }
            })
        })
        .collect();

    for handle in thread_handles {
        handle.join().unwrap();
    }
    let min_polymer = *min_polymer.lock().unwrap();
    min_polymer
}

fn main() {
    println!("{}", day5_part2(&day5::read_input()));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn day5_part2_examples() {
        assert_eq!(day5_part2("dabAcCaCBAcCcaDA"), 4);
    }
}
